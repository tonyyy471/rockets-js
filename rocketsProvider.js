const rocketsProvider = {
    getRockets: function () {
        const self = this;
        return new Promise(function (resolve, reject) {
            self.requestRockets(resolve, reject);
        });
    },

    requestRockets: function (resolve, reject) {
        const self = this;
        const request = new XMLHttpRequest();
        request.open('get', 'https://api.spacexdata.com/v2/rockets');
        request.onload = function () {
            if (this.status >= 200 && this.status < 300) {
                resolve(JSON.parse(this.responseText));
            } else {
                reject(self.rejectObject(this));
            }
        };
        request.onerror = function () {
            reject(self.rejectObject(this));
        };
        request.send();
    },

    rejectObject: function (request) {
        return {
            status: request.status,
            statusText: request.responseText
        };
    }
};