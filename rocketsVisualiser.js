const rocketsVisualiser = {
    rocketsProvider: rocketsProvider,

    visualise: async function () {
        const rockets = await this.rocketsProvider.getRockets();
        this.load(rockets);
        this.move(rockets);
    },

    load: function (rockets) {
        this.rocketsCount = rockets.length;
        for (let i = 0; i < rockets.length; i++) {
            const rocket = rockets[i];
            this.loadRocket(rocket);
        }
    },

    loadRocket: function (rocket) {
        //For illustration...
        rocket.first_stage.fuel_amount_tons = Math.ceil(Math.random() * 10);
        rocket.second_stage.fuel_amount_tons = Math.ceil(Math.random() * 10);
        //

        const rocketContainer = this.loadRocketContainer(rocket.id);

        this.loadRocketPart(rocketContainer, 'assets/rocket_top.png', rocket.id + 'top');
        this.loadRocketPart(rocketContainer, 'assets/rocket_bottom.png', rocket.id + 'bottom');
        this.loadRocketPart(rocketContainer, 'assets/thrust.png', rocket.id + 'thrust', true);
        this.loadRocketFuel(rocketContainer, rocket);
    },

    loadRocketContainer: function (id) {
        const rocketContainer = document.createElement('span');
        rocketContainer.id = id;
        this.styleRocketContainer(rocketContainer);
        document.body.appendChild(rocketContainer);
        return rocketContainer;
    },

    styleRocketContainer: function (rocketContainer) {
        rocketContainer.style.display = 'inline-block';
        rocketContainer.style.width = (100 / this.rocketsCount) + '%';
        rocketContainer.style.position = 'relative';
        rocketContainer.style.top = (window.innerHeight - 210) + 'px';
    },

    loadRocketPart: function (rocketContainer, src, id, partIsThrust) {
        const rocketPart = document.createElement('img');
        rocketPart.src = src;
        rocketPart.id = id;
        this.styleRocketPart(rocketPart, partIsThrust);
        rocketContainer.appendChild(rocketPart);
    },

    styleRocketPart: function (part, partIsThrust) {
        part.style.display = 'block';
        part.style.marginLeft = 'auto';
        part.style.marginRight = 'auto';

        if (partIsThrust)
            this.styleThrust(part);
    },

    styleThrust: function (part) {
        part.style.animation = 'pulse 1s infinite';
        part.style.animationDirection = 'normal';
        part.style.webkitAnimationName = 'pulse';
        part.style.animationName = 'pulse';
    },

    move: function (rockets) {
        for (let i = 0; i < rockets.length; i++) {
            this.moveRocket(rockets[i]);
        }
    },

    moveRocket: function (rocket) {
        const rocketContainer = document.getElementById(rocket.id);
        let position = (window.innerHeight - 210);

        const interval = setInterval(function () {
            if (position === 0)
                clearInterval(interval);
            position--;
            rocketContainer.style.top = position + 'px';
        }, 15);
    },

    loadRocketFuel: function (rocketContainer, rocket) {
        const fuelContainer = this.loadRocketFuelContainer(rocketContainer);
        this.manageRocketFuel(fuelContainer, rocket);
    },

    loadRocketFuelContainer: function (rocketContainer) {
        const fuelContainer = document.createElement('i');
        this.styleRocketFuelContainer(fuelContainer);
        rocketContainer.appendChild(fuelContainer);
        return fuelContainer;
    },

    styleRocketFuelContainer: function (fuelContainer) {
        fuelContainer.style.display = 'block';
        fuelContainer.style.marginLeft = 'auto';
        fuelContainer.style.marginRight = 'auto';
        fuelContainer.style.fontSize = '20px';
        fuelContainer.style.textAlign = 'center';
        fuelContainer.style.color = 'white';
    },

    manageRocketFuel: function (fuelContainer, rocket) {
        const self = this;
        let firstStageIsDepleted = false;
        let secondStageIsDepleted = false;

        const interval = setInterval(function () {
            if (!firstStageIsDepleted) {
                if (rocket.first_stage.fuel_amount_tons <= 0) {
                    self.hide(rocket.id + 'bottom');
                    firstStageIsDepleted = true;
                } else {
                    self.displayRocketFuel(fuelContainer, rocket.name, 'First', rocket.first_stage.fuel_amount_tons);
                    rocket.first_stage.fuel_amount_tons--;
                }
            } else if (!secondStageIsDepleted) {
                if (rocket.second_stage.fuel_amount_tons <= 0) {
                    secondStageIsDepleted = true;
                } else {
                    self.displayRocketFuel(fuelContainer, rocket.name, 'Second', rocket.second_stage.fuel_amount_tons);
                    rocket.second_stage.fuel_amount_tons--;
                }
            } else {
                self.hide(rocket.id, true);
                self.rocketsCount--;
                clearInterval(interval);

                if (self.rocketsCount === 0)
                    self.loadSuccessScreen();
            }
        }, 1000);
    },

    displayRocketFuel: function (fuelContainer, rocketName, stage, fuelAmount) {
        fuelContainer.innerText = `${rocketName}\n ${stage} stage: ${Math.floor(fuelAmount)} tons of fuel left.`
    },

    hide: function (id, wholeRocket) {
        if (wholeRocket) {
            document.getElementById(id).style.visibility = 'hidden';
            return;
        }
        document.getElementById(id).style.display = 'none';
    },

    loadSuccessScreen: function () {
        const successMessageContainer = this.loadSuccessContainer();
        const successMessage = this.loadSuccessMessage();
        successMessageContainer.appendChild(successMessage);
        document.body.appendChild(successMessageContainer);

        const replayButtonContainer = this.loadSuccessContainer();
        const replayButton = this.loadReplayButton();
        replayButtonContainer.appendChild(replayButton);
        document.body.appendChild(replayButtonContainer);
    },

    loadSuccessContainer: function () {
        const messageContainer = document.createElement('span');
        this.styleSuccessContainer(messageContainer);
        return messageContainer;
    },

    styleSuccessContainer(messageContainer) {
        messageContainer.style.width = '50%';
        messageContainer.style.height = '600px';
        messageContainer.style.textAlign = 'center';
        messageContainer.style.cssFloat = 'left';
        messageContainer.style.display = 'inline-flex';
        messageContainer.style.justifyContent = 'center';
        messageContainer.style.alignItems = 'center';
    },

    loadSuccessMessage: function () {
        const successMessage = document.createElement('img');
        this.styleSuccessMessage(successMessage);
        successMessage.src = 'assets/success.png';
        return successMessage;
    },

    styleSuccessMessage: function (successMessage) {
        successMessage.style.width = '700px';
        successMessage.style.height = '600px';
    },

    loadReplayButton: function () {
        const replayButton = document.createElement('button');
        this.styleReplayButton(replayButton);
        replayButton.addEventListener('click', () => location.reload());
        return replayButton;
    },

    styleReplayButton: function (replayButton) {
        replayButton.style.width = '200px';
        replayButton.style.height = '200px';
        replayButton.innerText = 'REPLAY';
        replayButton.style.fontSize = '35px';
        replayButton.style.borderRadius = '50%';
        replayButton.style.color = 'darkblue';
    }
};